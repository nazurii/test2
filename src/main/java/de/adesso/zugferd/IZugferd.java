package de.adesso.zugferd;

public interface IZugferd {
    String sourcePath = "";
    String targetPath = "";

    void xmlToPdf(String xmlName, String pdfName, String resultName);

    void xmlToPdfWithPaths(String xmlSource, String pdfSource, String resultName);

    void xmlToPdf(String xmlName, String pdfName, String resultName, String profile);

    void xmlToPdfWithPaths(String xmlSource, String pdfSource, String resultName, String profile);

    void changeSourcePath(String path);

    void changeTargetPath(String path);
}
