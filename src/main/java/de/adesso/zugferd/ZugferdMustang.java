package de.adesso.zugferd;

import org.mustangproject.Invoice;
import org.mustangproject.ZUGFeRD.Profiles;
import org.mustangproject.ZUGFeRD.ZUGFeRD2PullProvider;
import org.mustangproject.ZUGFeRD.ZUGFeRDExporterFromA3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import static de.adesso.zugferd.Constants.*;

public class ZugferdMustang implements IZugferd{
    String sourcePath = "src/test/java/de/adesso/zugferd/source/";
    String targetPath = "src/test/java/de/adesso/zugferd/target/";
    private static final Logger LOGGER = Logger.getLogger(ZugferdMustang.class.getName());

    @Override
    public void xmlToPdf(String xmlName, String pdfName, String resultName) {
        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(sourcePath+pdfName+".pdf"))
        {
            String xmlString = new String(Files.readAllBytes(Paths.get(sourcePath+xmlName+".xml")));
            ze.setXML(xmlString.getBytes());
            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void xmlToPdfWithPaths(String xmlSource, String pdfSource, String resultName) {
        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(pdfSource+".pdf"))
        {
            String xmlString = new String(Files.readAllBytes(Paths.get(xmlSource+".xml")));
            ze.setXML(xmlString.getBytes());
            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override //TODO profile
    public void xmlToPdf(String xmlName, String pdfName, String resultName, String profile) {
        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(sourcePath+pdfName+".pdf")
                .setProfile(Profiles.getByName(profile,2)))

        {
            String xmlString = new String(Files.readAllBytes(Paths.get(sourcePath+xmlName+".xml")));
            ze.setXML(xmlString.getBytes());
            //ze.setProfile(Profiles.getByName(BASIC, 2));

            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override //TODO profile
    public void xmlToPdfWithPaths(String xmlSource, String pdfSource, String resultName, String profile) {

        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(pdfSource+".pdf"))
        {
            String xmlString = new String(Files.readAllBytes(Paths.get(xmlSource+".xml")));
            ze.setXML(xmlString.getBytes());
            ze.setProfile(Profiles.getByName(BASIC, 2));
            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeSourcePath(String path) {
        this.sourcePath = path;
    }

    @Override
    public void changeTargetPath(String path) {
        this.targetPath = path;
    }

    public void invoiceToPdf(Invoice invoice, String pdfName, String resultName) {
        String xmlString = invoiceToXml(invoice);

        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(sourcePath+pdfName+".pdf"))
        {

            ze.setXML(xmlString.getBytes());
            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void invoiceToPdfByPath(Invoice invoice, String pdfSource, String resultName){
        String xmlString = invoiceToXml(invoice);

        try(ZUGFeRDExporterFromA3 ze = new ZUGFeRDExporterFromA3().setProducer(PRODUCER)
                .setCreator(System.getProperty(CREATOR))
                .load(pdfSource+".pdf"))
        {
            ze.setXML(xmlString.getBytes());
            LOGGER.log(Level.FINE, "Writing ZUGFeRD-PDF");
            ze.export(targetPath+resultName+".pdf");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    //ZUGFeRD 2 BASIC Profile
    public String invoiceToXml(Invoice invoice){
        ZUGFeRD2PullProvider zf2p = new ZUGFeRD2PullProvider();
        zf2p.setProfile(Profiles.getByName(BASIC, 2));
        zf2p.generateXML(invoice);
        return new String(zf2p.getXML());
    }

}
