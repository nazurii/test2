package de.adesso.zugferd;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mustangproject.*;
import org.mustangproject.ZUGFeRD.ZUGFeRDImporter;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static de.adesso.zugferd.Constants.BASIC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ZugferdApplicationTests {

	ZugferdMustang zm = new ZugferdMustang();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String orgname = "Bei Spiel GmbH";
	String number = "RE-20201121/508";
	Invoice i;
	{
		try {
			i = new Invoice()
					.setDueDate(sdf.parse("2020-12-12"))
					.setIssueDate(sdf.parse("2020-11-21"))
					.setDeliveryDate(sdf.parse("2020-11-10"))
					.setSender(new TradeParty(orgname, "Ecke 12", "12345", "Stadthausen", "DE")
							.addBankDetails(new BankDetails("DE88200800000970375700", "COBADEFFXXX")
									.setAccountName("Max Mustermann")).addVATID("DE136695976"))
					.setRecipient(new TradeParty("Theodor Est", "Bahnstr. 42", "88802", "Spielkreis", "DE")
							.setContact(new Contact("Ingmar N. Fo", "(555) 23 78-23", "info@localhost.local")).setID("2"))
					.setNumber(number)
					.setReferenceNumber("AB321")
					.addItem(new Item(new Product("Design (hours)", "Of a sample invoice", "HUR",
							new BigDecimal(7)), new BigDecimal("160.00"), new BigDecimal("1.0")))
					.addItem(new Item(new Product("Ballons", "various colors, ~2000ml", "H87",
							new BigDecimal(19)), new BigDecimal("0.79"), new BigDecimal("400.0")))
					.addItem(new Item(new Product("Hot air „heiße Luft“ (litres)", "", "LTR",
							new BigDecimal(19)), new BigDecimal("0.025"), new BigDecimal("800.0")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}


	@Test
	void xmlToPdfTest(){
		zm.xmlToPdf("factur-x", "blanko", "resultPdf");
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
	}

	@Test
	void xmlToPdfWithPaths(){
		zm.xmlToPdfWithPaths(zm.sourcePath+"factur-x", zm.sourcePath+"blanko","resultPdf");
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
	}

	//@Test
	void xmlToPdfProfileTest(){
		zm.xmlToPdf("factur-x", "blanko", "resultPdf", BASIC);
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
		try {
			assertEquals(zi.getVersion(), 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(zi.getZUGFeRDProfil(), BASIC);
	}

	//@Test
	void xmlToPdfWithPathsProfileTest(){
		zm.xmlToPdfWithPaths(zm.sourcePath+"factur-x", zm.sourcePath+"blanko","resultPdf", BASIC);
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
		try {
			assertEquals(zi.getVersion(), 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(BASIC, zi.getZUGFeRDProfil());
	}


	@AfterEach
	void deleteResultPdfs(){
		File file = new File(zm.targetPath+"resultPdf.pdf");
		file.delete();
	}

	@Test
	void invoiceToPdfTest(){
		zm.invoiceToPdf(i, "blanko", "resultPdf");
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
	}

	@Test
	void invoiceToPdfByPathTest(){
		zm.invoiceToPdfByPath(i, zm.sourcePath+"blanko", "resultPdf");
		ZUGFeRDImporter zi = new ZUGFeRDImporter(zm.targetPath+"resultPdf.pdf");

		assertTrue(zi.getUTF8().contains("DE88200800000970375700")); //IBAN
		assertTrue(zi.getUTF8().contains("Max Mustermann")); //account holder
		assertTrue(zi.getUTF8().contains("<rsm:CrossIndustryInvoice"));
		assertTrue(zi.getUTF8().contains("EUR")); //default invoice currency
		assertTrue(zi.getUTF8().contains("AB321")); //buyer Reference

		assertEquals("571.04", zi.getAmount()); //grand total amount
		assertEquals(zi.getHolder(), orgname);
		assertEquals(zi.getForeignReference(), number);
	}

	@Test
	void invoiceToXmlTest(){
		String xml = zm.invoiceToXml(i);
		assertTrue(xml.contains("DE88200800000970375700")); //IBAN
		assertTrue(xml.contains("Max Mustermann")); //account holder
		assertTrue(xml.contains("<rsm:CrossIndustryInvoice"));
		assertTrue(xml.contains("EUR")); //default invoice currency
		assertTrue(xml.contains("AB321")); //buyer Reference
	}
}
